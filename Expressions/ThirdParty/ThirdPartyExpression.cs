﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Microsoft.Win32.SafeHandles;
//using System.Runtime.InteropServices;
using Expressions.Interfaces;


/// <summary>
/// All most all the code in this file comes from https://codereview.stackexchange.com/questions/93997/expression-calculator
/// I am not claming to have writen it I am simple using it as an example
/// Credit goes to https://codereview.stackexchange.com/users/33863/rookie
/// The code is very BUGGIE. 
/// I might fix some of the mentioned bugs to get my example working but for demonstration perpuses it is all I need.
/// </summary>
namespace Expressions.ThirdParty
{

    public class ThirdPartyExpression : IExpression
    {
        public string Description { get ; set ; }

        public ThirdPartyExpression()
        {
            Description = $"My simple calculator using Thrid party code From https://codereview.stackexchange.com/questions/93997/expression-calculator ";
        }

        public string Calc(string infix)
        {
            var exp = new CalculatorExpression(infix);
            double val = exp.Evaluate;
            return val.ToString();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ThirdPartyExpression() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }


    /// <summary>
    /// Represents a binary operation between two doubles 
    /// </summary>
    internal delegate double Operator(double x, double y);

    /// <summary>
    /// Represents a node in BinaryExpression tree
    /// </summary>
    internal class BinaryExpression
    {
        protected BinaryExpression()
        {
        }

        /// <summary>
        /// Constructs a BinaryExpression tree from left and right subtrees
        /// to be combined with an operator
        /// </summary>
        public BinaryExpression(BinaryExpression left, BinaryExpression right,
            Operator op)
        {
            Left = left; Right = right; Operator = op;
        }

        /// <summary>
        /// Returns the value of the tree 
        /// </summary>
        public virtual double Value
        {
            get
            {
                return Operator(Left.Value, Right.Value);
            }
            protected set { } // only child classes (i.e. BinaryAtomic) should be able to set Value
        }

        public BinaryExpression Left;
        public BinaryExpression Right;
        public Operator Operator;
    }

    /// <summary>
    /// Represents a leaf in BinaryExpression tree
    /// </summary>
    internal class BinaryAtomic : BinaryExpression
    {
        protected BinaryAtomic()
        {
        }

        /// <summary>
        /// Constructs a leaf 
        /// </summary>
        public BinaryAtomic(double value)
        {
            Value = value;
        }

        /// <summary>
        /// Returns the leaf value
        /// </summary>
        public override double Value
        {
            get;
            protected set;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }


    /// <summary>
    /// Represents an infix mathematical expression involving +, -, *, /, ()
    /// </summary>
    public class CalculatorExpression
    {
        /// <summary>
        /// Constructs an expression tree for infix input
        /// </summary>
        public CalculatorExpression(string input)
        {
            var nodes = new Stack<BinaryExpression>();
            foreach (var c in ToPostFix(input))
            {
                var s = c.ToString();
                double n;
                if (double.TryParse(s, out n))
                {
                    nodes.Push(new BinaryAtomic(n));
                }
                else if (IsOperator(s))
                {
                    var r = nodes.Pop();
                    var l = nodes.Pop();
                    nodes.Push(new BinaryExpression(l, r, Operators[s]));
                }
            }
#if DEBUG
            System.Diagnostics.Debug.Assert(nodes.Count == 1);
#endif
 
            tree = nodes.Pop();
        }

        /// <summary>
        /// Returns the value of the expression
        /// </summary>
        public double Evaluate
        {
            get { return tree.Value; }
        }

        private BinaryExpression tree;

        /// <summary>
        /// Helper to generate postfix notation. In the constructor, 
        /// the input is first converted to postfix; the postfix is then
        /// used to create a BinaryExpression tree
        /// </summary>
        private static string ToPostFix(string input)
        {
            var postfix = new StringBuilder();

            // A stack is used to hold the operators 
            // because we don't know when we've reached the 
            // end (right operand) of an expression 
            var ops = new Stack<string>();
            foreach (var c in input)
            {
                // When we see an operator, we can pop anything
                // with higher precedence onto the infix. 
                // We do the operations with higher priority first
                var s = c.ToString();
                if (IsOperator(s))
                {
                    while (ops.Count > 0 && Precedence(ops.Peek()) >= Precedence(s))
                        postfix.Append(ops.Pop());
                    ops.Push(s);
                }
                else
                {
                    // When we see an open parenthesis, 
                    // we push the paren onto the stack and wait until we
                    // see a closing parenthesis. Then we know
                    // that the parenthesized expression is complete.
                    // While we haven't seen that first opening paren, everything on 
                    // the operator stack is popped (part of the inner expression);
                    // precedence will be taken care of for us by virtue of the above if-statement
                    switch (s)
                    {
                        case "(":
                            ops.Push(s);
                            break;

                        case ")":
                            var top = ops.Pop();
                            while (top != "(")
                            {
                                postfix.Append(top);
                                top = ops.Pop();
                            }
                            break;
                        default:
                            postfix.Append(s); // operands always go onto the infix 
                            break;
                    }
                }
            }

#if DEBUG
            System.Diagnostics.Debug.Assert(!ops.Any(x => x == "("));
#endif
 
            while (ops.Count > 0)
                postfix.Append(ops.Pop());
            return postfix.ToString();
        }

        /// <summary>
        /// The only operations currently supported are 
        /// -, +, *, /
        /// </summary>
        private static bool IsOperator(string op)
        {
            return Operators.ContainsKey(op);
        }

        /// <summary>
        /// Multiplication and division have 
        /// higher precedence than +/-
        /// </summary>
        private static int Precedence(string op)
        {
            if (op == "-" || op == "+")
                return 1;
            if (op == "*" || op == "/")
                return 2;
            return 0;
        }

        private static Dictionary<string, Operator> Operators = new Dictionary<string, Operator>()
    {
        {"+", (x, y) => x + y},
        {"-", (x, y) => x - y},
        {"*", (x, y) => x * y},
        {"/", (x, y) => x / y},
    };
    }


}
