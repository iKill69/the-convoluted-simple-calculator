﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expressions.Interfaces
{
    public interface IExpression: IDisposable
    {
        string Description { get; set; }
        string Calc(string infix);
    }

}
