﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Expressions.Interfaces;
using System.Text;

/// <summary>
/// This is a very quick and dirty class to demonstarte using online API to process expressions
/// </summary>
namespace Expressions.Online
{
    public class OnlineExpression : IExpression
    {
        public string Description { get ; set ; }
         
        public OnlineExpression() {
            Description = $"My simple calculator using REST API at http://api.mathjs.org/";
        }


        public string Calc(string infix)
        {
            //You should never really do this moving from Async to sync but sometimes you have to.
            var result = Task.Run(async () => await GetAsync(infix)).Result;
            return result;

        }
         
        /// <summary>
        /// Quick and dirty method to call the web api to do the expression calculation
        /// </summary>
        /// <param name="infix">Expression string to be calculated</param>
        /// <returns></returns>
        public async Task<string> GetAsync(string infix)
        {
            string result = "";
            var url = @"http://api.mathjs.org/v4/";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            MyRequest r = new MyRequest();
            r.expr =   infix ;

            var json = JsonConvert.SerializeObject(r);
            var response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                var stringResult = await response.Content.ReadAsStringAsync();
                var myResult = JsonConvert.DeserializeObject<MyResult>(stringResult);
                if (myResult.error != null)
                {
                    throw new Exception(myResult.error); //error sent back from the server about the expression
                }
                else {
                    result = myResult.result;  
                }
                
            }

            return result;
        }


        internal class MyRequest
        {
            public string expr { get; set; }
            public int precision { get; set; }
        }

        internal class MyResult
        {
            public string result { get; set; }
            public string error { get; set; }
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~OnlineExpression() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
