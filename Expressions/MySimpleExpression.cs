using System;
//using Microsoft.Win32.SafeHandles;
//using System.Runtime.InteropServices;
using System.Linq;
using System.Text.RegularExpressions;
using Expressions.Interfaces;

namespace Expressions
{
    

    public class MySimpleExpression : IExpression
    {
        // Flag: Has Dispose already been called?
        bool disposed = false;
        // Instantiate a SafeHandle instance. 
        //SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        private const char StartSubExpression = '(';
        private const char EndSubExpression = ')';
        private string[] Operators = new string[4] { "+", "-", "/", "*" };

        public string Description { get; set; }

        #region Constructors and Finalizers

        public MySimpleExpression()
        {
            Description = $"My simple calculator in a very complicated way. Operators supported '),(,{string.Join(", ", Operators) }'";
        }

        //~MySimpleExpression()
        //{
        //    //Finalizers clean up here we dont need this yet
        //}

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //handle.Dispose(); //We dont need this yet
                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            disposed = true;
        }

        #endregion

        public string Calc(string infix)
        {

            //Prep the string for processing no spaces
            var infix_NoSpace = infix.Replace(" ", "");
             
            Regex regex = new Regex(@"\s*([.)(-+/*0-9])\s*");

            if (!regex.IsMatch(infix_NoSpace)) //Check for 
            {
                throw new ArgumentException("Expression invalid char [ ( ) - + / * 0-9].  Please see help. --h");
            }

            var iLength = infix_NoSpace.Length;
            return CalcSubExpressions(infix_NoSpace, 0, out iLength);

        }

        //BODMAS
        private string CalcSubExpressions(string infixPart, int expStartIndex, out int expEndIndex)
        {
            // 5+20/2 = 15
            // (1+4)*3 = 15
            // (1+4)*6/2 = 15
            // (1+4)*(2+1) = 15
            // ((5-2*2)+4)*(2+1) = 15
            // (1+8/(1+1))*(2+1) = 15
            // (1+8/(1+1))*((-3)+6) = 15  //Test Neg last Might not work????

            string result = "";
            string expPart = "";

            expEndIndex = infixPart.Length-1; //Just in case nothing is sent through

            char[] infixPartChar = infixPart.ToCharArray();
            for (var i = expStartIndex; infixPartChar.Length >= (i + 1); i++)
            {

                if (infixPartChar[i] == StartSubExpression)
                {
                    //Start of a new sub expression

                    expPart += CalcSubExpressions(infixPart, (i + 1), out i);
                }
                else if (infixPartChar[i] == EndSubExpression)
                {
                    //End of a sub expression so Calc the result
                    expEndIndex = i++; //send out the start index of the next part of the expression
                    break; //Exit the recursive loop to calc the result 
                }
                else
                {
                    //Ad the char to the Sub expression ready for calculation
                    expPart += infixPartChar[i].ToString(); //Concat char
                }
            }

            //Run the calc for the expression
            result = CalcSubExp(expPart).ToString();

            return result;

        }

        //BODMAS
        //This might not work with negitive numbers???? TEST
        private double CalcSubExp(string expPart)
        { 
            // Operators[0]: // +
            // Operators[1]: // -
            // Operators[2]: // /
            // Operators[3]: // *

            double result = 0; //starting point

            #region Add Subtract 

            //Split out addition and subtraction so that we can do them last
            var AddExp = expPart.Split(Operators[0].ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (AddExp.Length > 1) //If we are down to 1 number then we must return it as the answer
            { 
                result = CalcSubExp(AddExp[0]); //Start off with the first number

                //Do each of the divides
                for (var i = 1; i <= (AddExp.Length - 1); i++)
                {
                    var add = CalcSubExp(AddExp[i]);
                    result = result + add;
                }
                 
                //After processing exit recursive loop
                return result;
            }

            var SubExp = expPart.Split(Operators[1].ToCharArray());
            if (SubExp.Length > 1)//If we are down to 1 number then we must return it as the answer
            { 
                result = CalcSubExp(SubExp[0]); //Start off with the first number

                //Do each of the divides
                for (var i = 1; i <= (SubExp.Length - 1); i++)
                {
                    var sub = CalcSubExp(SubExp[i]);
                    result = result - sub;
                }
                 
                //After processing exit recursive loop
                return result;
            }

            #endregion

            #region Multiplication and Division

            //Multiplication and division must be done first 
            var MultExp = expPart.Split(Operators[3].ToCharArray());
            if (MultExp.Length > 1)//If we are down to 1 number then we must return it as the answer
            {

                result = CalcSubExp(MultExp[0]); //Start off with the first number

                //Do each of the divides
                for (var i = 1; i <= (MultExp.Length - 1); i++)
                {
                    var mut = CalcSubExp(MultExp[i]);
                    result = result * mut;
                }

                //After processing exit recursive loop
                return result;
            }

            //We are at the bottom of the nesting here
            var DivExp = expPart.Split(Operators[2].ToCharArray());
            if (DivExp.Length > 1)//If we are down to 1 number then we must return it as the answer
            {

                //At this point there should only be numbers
                result = PrepDouble(DivExp[0]); //Start off with the first number

                //Do each of the divides
                for (var i = 1; i <= (DivExp.Length - 1); i++)
                {
                    var div = PrepDouble(DivExp[i]); //At this point there should only be numbers
                    if (div == 0)
                    {
                        throw new DivideByZeroException();
                    }
                    result = result / div;
                }

                //After processing exit recursive loop
                return result;
            }

            #endregion

            //This should happen when you are down to the final digits with no operators to calculate
            result = PrepDouble(expPart);
             
            return result;

        }



        private double PrepDouble(string d)
        {
            try
            {
                //quick and dirty fix for nulls and zero length string and negitve numbers
                return double.Parse($"0{d}"); 
            }
            //catch(ArgumentNullException ex)
            //catch(FormatException ex)
            //catch(OverflowException ex)
            catch (Exception ex)
            {
                throw new Exception("There is a problem with your Expression. Only Numbers can be calculated", ex);
            }
        }


        /// <summary>
        /// The only operations currently supported are 
        /// -, +, *, /
        /// </summary>
        private bool IsOperator(string op)
        {
            return Operators.Contains(op);
        }


    }

}





