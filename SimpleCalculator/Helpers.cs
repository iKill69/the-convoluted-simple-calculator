﻿using System;

namespace SimpleCalculator
{
    public enum AppLoggging { Info = 0, Verbose = 1 }
    public enum AppMode { MySimple = 0, ThirdParty = 1 , Online=2}

    public enum TargetPlatformName { Win = 0, Unix = 1, MacOS = 2, XBox = 4, Unknown = 5 }

    public static class AppConfig
    {
        //public static AppMode ApplicationMode = AppMode.Server;
        public static bool Verbose = false;
        public static AppMode ApplicationMode = AppMode.MySimple;
    }
     
    public static class Helpers
    {

        /// <summary>
        /// I will use this when I need different code paths based on target OS type
        /// </summary>
        public static TargetPlatformName TargetPlatform
        {
            get
            {
                switch (Environment.OSVersion.Platform)
                {
                    case PlatformID.Win32S:
                    case PlatformID.Win32Windows:
                    case PlatformID.Win32NT:
                    case PlatformID.WinCE:
                        return TargetPlatformName.Win;
                    case PlatformID.Unix:
                    case PlatformID.MacOSX:
                        return TargetPlatformName.Unix;
                    case PlatformID.Xbox:
                        return TargetPlatformName.XBox;
                    default:
                        return TargetPlatformName.Unknown;
                }
            }
        }

        public static void MyWriteLine(string format, AppLoggging LogLevel = AppLoggging.Info, params object[] arg)
        {
            if (LogLevel == AppLoggging.Verbose)
            {
                if (AppConfig.Verbose)
                {
                    //Only log Verbose msg if app is in the corret mode.
                    Console.WriteLine($"VERBOSE: {format}", arg);
                }
            }
            else if (LogLevel == AppLoggging.Info)
            {
                Console.WriteLine(format, arg);
            }

        }
        public static void MyWriteLine(string format, AppLoggging LogLevel = AppLoggging.Info)
        {

            if (LogLevel == AppLoggging.Verbose)
            {
                if (AppConfig.Verbose)
                {
                    //Only log Verbose msg if app is in the corret mode.
                    Console.WriteLine($"VERBOSE: {format}");
                }
            }
            else if (LogLevel == AppLoggging.Info)
            {
                Console.WriteLine(format);
            }

        }
         
    }


}


 