﻿using System;
using Expressions;
using Expressions.ThirdParty;
using Expressions.Online;
using Expressions.Interfaces;
using System.Text.RegularExpressions;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            
            try
            {

#if DEBUG
                //easy place to change startup test parameters for Dev and Debug
                //faster than setting up and changing project settings each time

                //args = new string[2] { "--m:s", "--e:(1+4)*3" };
                //args = new string[2] { "--m:3", "--e:(1+4)*3" };
                //args = new string[2] { "--m:o", "--e:(1+4)*3" };

                //args = new string[2] { "--m:s", "--e:5+20/2" };
                //args = new string[2] { "--m:3", "--e:5+20/2" };
                //args = new string[2] { "--m:o", "--e:5+20/2" };

                //args = new string[2] { "--m:s", "--e:(1+8/(1+1))*(2+1)" };
                //args = new string[2] { "--m:s", "--e:(1+8/(1+1))*(-3+6)" }; //negitive number test
                //args = new string[2] { "--m:s", "--e:(1+8/(1+1))*(6+-3)" }; //negitive number test

                //args = new string[2] { "--m:s", "--e:(1+8.0/(1+1))*((-3)+6)" }; //negitive number test
                //args = new string[2] { "--m:o", "--e:(1+8.0/(1+1))*((-3)+6)" }; //negitive number test

                //args = new string[2] { "--m:s", "--e:5*4-10/2+(0*(1+0.1234))" };
                //args = new string[2] { "--m:o", "--e:5*4-10/2+(0*(1+0.1234))" };

                //args = new string[2] { "--m:s", "--e:15/(10-5-5)" };  //DIV BY ZERO TEST
                //args = new string[2] { "--m:o", "--e:15/(10-5-5)" };  //DIV BY ZERO TEST
                
                //args = new string[2] { "--m:s", "--e:-1*(-20+5)" }; //BUG TO FIX
#endif
                string infix ="";
                bool expressionRequired = true;

                foreach (var arg in args)
                {
                    var a = arg.Split(':');

                    switch (a[0])
                    {
                        case "--m":  //Application Mode
                            if (a.Length == 2) 
                            {
                                switch (a[1].Trim().ToLower())
                                {
                                    case "s":
                                        AppConfig.ApplicationMode = AppMode.MySimple;
                                        break;
                                    case "3":
                                        AppConfig.ApplicationMode = AppMode.ThirdParty;
                                        break;
                                    case "o":
                                        AppConfig.ApplicationMode = AppMode.Online;
                                        break;
                                    default:
                                        AppConfig.ApplicationMode = AppMode.MySimple;
                                        break;
                                }
                            }
                            else
                            {
                                throw new ArgumentException("Mode invalide. Please see help. --h");
                            }
                            break;
                        case "--e":  //Expression
                            if (a.Length == 2)
                            {
                                infix = a[1].Trim();
                                Regex regex = new Regex(@"\s*([.)(-+/*0-9])\s*");

                                if (!regex.IsMatch(infix)) //Check for 
                                { 
                                    throw new ArgumentException("Expression invalid char [ ( ) - + / * 0-9].  Please see help. --h");
                                }
                                
                            }
                            else
                            {
                                throw new ArgumentException("Mode invalide. Please see help. --h");
                            }
                            break;
                        case "--v":
                            AppConfig.Verbose = true;
                            Helpers.MyWriteLine("Verbose mode enabled", AppLoggging.Verbose);
                            break;
                        case "--h":
                            displayHelp();
                            expressionRequired = false;
                            break;
                        default:
                            throw new ArgumentException("This could be the result of spaces in your expression. Please see help. --h");
                           
                    }
                }

#if DEBUG
                //easy place to change startup test parameters for Dev and Debug after args processing

#endif

                if (expressionRequired)
                {
                    switch (AppConfig.ApplicationMode)
                    {
                        case AppMode.MySimple:

                            Helpers.MyWriteLine("Execute as MySimple", AppLoggging.Verbose);

                            using (IExpression exp = new MySimpleExpression())
                            {
                                Helpers.MyWriteLine("Description: {0}", AppLoggging.Verbose, exp.Description);

                                var result = exp.Calc(infix);

                                Helpers.MyWriteLine("Result:{2}{0}={1}", AppLoggging.Info, infix, result, Environment.NewLine);
                            }
                             
                            break;
                        case AppMode.ThirdParty:
                             
                            Helpers.MyWriteLine("Execute as ThirdParty", AppLoggging.Verbose);

                            using (IExpression exp = new ThirdPartyExpression())
                            {
                                Helpers.MyWriteLine("Description: {0}", AppLoggging.Verbose, exp.Description);

                                var result = exp.Calc(infix);

                                Helpers.MyWriteLine("Result:{2}{0}={1}", AppLoggging.Info, infix, result, Environment.NewLine);
                            }
                            break;
                        case AppMode.Online:
                            Helpers.MyWriteLine("Execute as Online", AppLoggging.Verbose);

                            using (IExpression exp = new OnlineExpression())
                            {
                                Helpers.MyWriteLine("Description: {0}", AppLoggging.Verbose, exp.Description);

                                var result = exp.Calc(infix);

                                Helpers.MyWriteLine("Result:{2}{0}={1}", AppLoggging.Info, infix, result, Environment.NewLine);
                            }
                            break;
                        default:
                            //this should never happen really but just incase lets display help
                            displayHelp();
                            break;
                    }
                }
                 

            }
            catch (Exception err)
            {
                Console.WriteLine(err.ToString());
                displayHelp();
            }
            finally
            {

                Console.Write("Press <Enter> to exit... ");
                while (Console.ReadKey().Key != ConsoleKey.Enter) { }
                Console.WriteLine("Exit - All done!");
            }
        }



        private static void displayHelp()
        {
            Console.WriteLine("==================================");
            Console.WriteLine(" -- Help -- ");
            Console.WriteLine("-----------------------------");

            Console.WriteLine("--h          Display Help");
            Console.WriteLine("--v          Verbose Mode");
            Console.WriteLine("--e          Expression, Should not have any spaces and only the following Char are valid [ ( ) - + / * 0-9]. eg: --e:(1+4)*3");
            Console.WriteLine("--m          Start up Mode (s - Simple , 3 - 3rd Party, o - Online )");

            Console.WriteLine("-----------------------------");

            Console.WriteLine("Simple Example:              dotnet run SimpleCalculator.dll --m:s --e:(1+4)*5");
            Console.WriteLine("3rd Party Example:           dotnet run SimpleCalculator.dll --m:3 --e:(1+4)*5");
            Console.WriteLine("Online Example:              dotnet run SimpleCalculator.dll --m:o --e:(1+4)*5");
            Console.WriteLine("Simple Verbose Example:      dotnet run SimpleCalculator.dll --m:s --e:(1+4)*5 --v");

            Console.WriteLine("==================================");
        }
    }

}


 