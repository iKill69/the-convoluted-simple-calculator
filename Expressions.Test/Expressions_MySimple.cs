using Xunit;
using Expressions.Interfaces;

//Thanks Microsoft!!! 
//https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test
namespace Expressions.Test
{
    public class Expressions_MySimple
    {
        private readonly IExpression _expressionsService;

        public Expressions_MySimple()
        {
            _expressionsService = new MySimpleExpression();
        }

      

        #region Single_Fact_Example_TestCode
        [Fact]
        public void ReturnTrueGivenDividebyZero()
        {
            try {
                var result = _expressionsService.Calc("15/(10-5-5)");
                Assert.True(false, "DivideByZeroException expected");

            } catch (System.DivideByZeroException)
            {
                Assert.True(true, "DivideByZeroException success");
            }

        }

      

        

        #endregion

        #region Multiple_Fact_Example_TestCode
        
        [Theory]
        [InlineData("5+20/2")]
        [InlineData("(1+4)*3")]
        [InlineData("(1+4)*6/2")]   
        [InlineData("(1+4)*(2+1)")]   
        [InlineData("((5-2*2)+4)*(2+1)")]   
        [InlineData("(1+8/(1+1))*(2+1)")]   
        [InlineData("(1+8/(1+1))*((-3)+6)")]
        [InlineData("(1+8/(1+1))*(6+-3)")]
        [InlineData("5*4-10/2+(0*(1+0.1234))")]
        //[InlineData("-1*(-20+5)")]  //BUG NEGITIVE NUMBER BUG
        public void ReturnFalseGivenResultOtherThan15(string value)
        { 
            var result = !(double.Parse(_expressionsService.Calc(value)) == 15);
            Assert.False(result, $"{value} should equale 15");
        }

        #endregion


        //[Theory] 
        //[InlineData(2)] 
        //[InlineData(3)] 
        //[InlineData(5)] 
        //[InlineData(7)] 
        //public void ReturnTrueGivenPrimesLessThan10(int value) 
        //{ 
        //    var result = _expressionsService.IsPrime(value); 

        //    Assert.True(result, $"{value} should be prime"); 
        //} 

        //[Theory] 
        //[InlineData(4)] 
        //[InlineData(6)] 
        //[InlineData(8)] 
        //[InlineData(9)] 
        //public void ReturnFalseGivenNonPrimesLessThan10(int value) 
        //{ 
        //    var result = _expressionsService.IsPrime(value); 

        //    Assert.False(result, $"{value} should not be prime"); 
        //} 

    }
}


