# The Convoluted Simple Calculator #

**The Convoluted Simple Calculator demonstrates how you can make a simple task harder.**

This application is a dotnet Core cross platform expression calculator. It accepts Infix notation.
 
### Goals ###

* Demonstrate simple Dependency Injection 
* Build multiple working user interface using the same code (console, web, api) 
* Build a basic Unit test project for the main expressions library. 

### Not Goals ###

* Perfect bug free optimized application code
* Secure Web application
 
### What is this repository for? ###

* dotnet core 2.1 
 
### How do I get set up? ###

* Install Git from https://git-scm.com/book/en/v2/Getting-Started-Installing-Git 
* Configuration Git for first use https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup 
* Install dotnet Core https://www.microsoft.com/net/download/dotnet-core/sdk-2.1
* Install VSCode https://code.visualstudio.com/download 
* Download this source or clone to a folder. [git clone https://iKill69@bitbucket.org/iKill69/the-convoluted-simple-calculator.git]
* Open VSCode >> File >>> Open Folder... and browse to the new folder
* VSCode Terminal run [dotnet restore]
* [Ctrl + Shift + b] then [Enter] to build (you will need to build both the console and the web projects if you want to test them both)
* [F5] to run 

### Cross Platform build ###

VSCode Terminal or in the CLI application directory run the following 

* dotnet restore
* dotnet publish -c Release -r ubuntu.18.04-x64  
* dotnet publish -c Release -r osx.10.11-x64
* dotnet publish -c Release -r win10-x64  
* dotnet publish -c Release -r win-x86
* dotnet publish -c Release -r win10-arm
* dotnet publish -c Release -r linux-x64



### Expressions Assemble ###

This is a reusable assemble with multiple namespaces to demonstrate different ways to process an Infix notation expression and evaluate the result.
All classes inherit the same **IExpression** interface for later Dependency Injection.

** MySimpleExpression ** 

* My example using nested strings and recursive functions to implement BODMAS.
* I specifically avoided Reverse Polish notation because I wanted a different way to get the same result.
* There are still bugs with negative numbers

** ThirdPartyExpression **

* Third party code attempting to use the Reverse Polish notation 
* This code is buggy but it service its purpose as a third party. 
* It does not accept multi-digit numbers 

** OnlineExpression ** 

* Simple example of calling out to an API to do the calculation


### Console App Help ###

* --h          Display Help
* --v          Verbose Mode 
* --e          Expression, Should not have any spaces and only the following Char are valid [ ( ) - + / * 0-9]. eg: --e:(1+4)*3
* --m          Start up Mode (s - Simple , 3 - 3rd Party, o - Online )

* Help Example:              	dotnet run SimpleCalculator.dll --h
* Simple Example:              dotnet run SimpleCalculator.dll --m:s --e:(1+4)*5
* 3rd Party Example:           dotnet run SimpleCalculator.dll --m:3 --e:(1+4)*5
* Online Example:              dotnet run SimpleCalculator.dll --m:o --e:(1+4)*5
* Simple Verbose Example:      dotnet run SimpleCalculator.dll --m:s --e:(1+4)*5 --v
  
### Web App ###

* Only runs under HTTP so it is not secure.
* I have not applied Swagger to the API
* Built using the Reaxt Redux SPA template.
* See the appsettings.json file in the WebCalculator folder to change the Expressions library used.

Example:

  "ExpressionConfig": {
    "Lib": "MySimple"  
  }

**Lib values:**

* "MySimple" 	– My example using nested strings and recursive functions to implement BODMAS
* "3rd" 		– Third party code attempting to use the Reverse Polish notation 
* "Online"		– Simple example of calling out to an API to do the calculation
 
### Docker ###

I have setup a continues deploymnet pipleine using Bitbucket and Docker Hub.

The docker image is located here
https://hub.docker.com/r/ikill69/convoluted_calculator/

After installing docker you can follow these steps to see the example web interface

* docker pull ikill69/convoluted_calculator:ec3a711e80a42be7513731acd7ba8cda35df90cd
* docker run -p 8000:80  ikill69/convoluted_calculator:ec3a711e80a42be7513731acd7ba8cda35df90cd
* Browse to: http://localhost:8000/


### Known issues ###

 ** I am aware that using the terms ‘Expressions’ and ‘Expression’ could get messy with Linq.**

 ** THE WEB APP IS NOT SECURE**
 It does not run over HTTPS. It is for demo only

 ** Negitive number bug **
 [InlineData("-1*(-20+5)")]  //BUG NEGITIVE NUMBER BUG

### Related links ###
* https://en.wikipedia.org/?title=Infix_notation
* https://en.wikipedia.org/wiki/Reverse_Polish_notation
* https://docs.microsoft.com/en-us/dotnet/core/testing/unit-testing-with-dotnet-test
* https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1
* https://codereview.stackexchange.com/questions/93997/expression-calculator
* https://github.com/dotnet/dotnet-docker/tree/master/samples/aspnetapp
* https://confluence.atlassian.com/bitbucket/build-and-push-a-docker-image-to-a-container-registry-884351903.html
* https://confluence.atlassian.com/bitbucket/run-pipelines-manually-861242583.html


