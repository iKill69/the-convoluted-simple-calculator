FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

# copy csproj and restore as distinct layers
COPY *.sln .
COPY Expressions/*.csproj ./Expressions/
COPY Expressions.Test/*.csproj ./Expressions.Test/
COPY SimpleCalculator/*.csproj ./SimpleCalculator/
COPY WebCalculator/*.csproj ./WebCalculator/

RUN dotnet restore

# copy everything else and build app
COPY Expressions/. ./Expressions/
COPY Expressions.Test/. ./Expressions.Test/
COPY SimpleCalculator/. ./SimpleCalculator/
COPY WebCalculator/. ./WebCalculator/

WORKDIR /app/WebCalculator
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/WebCalculator/out ./
ENTRYPOINT ["dotnet", "WebCalculator.dll"]



