﻿const requestEval = 'REQUEST_EVAL';
const receiveEval = 'RECEIVE_EVAL';
const errorEval = 'ERROR_EVAL';
const initialState = { result: '', isLoading: false, error:'' };

export const actionCreators = {
    requestCalcEval: infixState => async (dispatch, getState) => {    
   
        try {

            console.log('=== dispatch REQUEST_EVAL');
            dispatch({ type: requestEval, infix:infixState });

            const body = {
                infix: infixState 
            }

            const url = 'api/SimpleCalculator/EvalExpression';
            const response = await fetch(url, {
                method: 'post',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body),
            });

            let responseJson = await response.json();

            if (response.ok) {
                console.log('=== dispatch RECEIVE_EVAL');
                 
                dispatch({ type: receiveEval, description: responseJson.description, result: responseJson.result });

                console.log('--- responseJson---');
                console.log(JSON.stringify(responseJson));

            } else {
                var errorMsg = response.statusText + ' : ' + responseJson.error;
                dispatch({ type: errorEval, error: errorMsg  });
            }
               
        } catch (error) {
            dispatch({ type: errorEval, error: error });

            console.error(error);
        } finally {
            console.log('dispatch complete');
        }
         
  }
};

export const reducer = (state, action) => {
  state = state || initialState;
     
    switch (action.type) {
        case 'REQUEST_EVAL':
            return {
                ...state,
                infix: action.infix,
                error: null,
                isLoading: true
            };
        case 'RECEIVE_EVAL':
            return {
                ...state,
                result: action.result,
                description: action.description,
                error: null,
                isLoading: false
            };
        case 'ERROR_EVAL':
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
            
        default:
            return state;
    }
     
};
