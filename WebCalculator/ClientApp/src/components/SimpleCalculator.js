import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import { Link } from 'react-router-dom';
import { actionCreators } from '../store/SimpleCalculatorEval';

class SimpleCalculator extends Component {
    //constructor(props) {
    //    super(props);
    //    //this.displayError = false;
    //}
       
    render() {
        const spacing = 10;
        const color = 'red';
        
        return (
            <div>
                <h1>Simple Expression Calculator</h1>
                <p>{this.props.description}</p>

                <table className="table" >
                    <thead>
                        <tr>
                            <th colSpan="2">Expression String</th>
                        </tr>
                       
                    </thead>
                    <tbody>
                        <tr>
                            <td>

                                <input
                                    name="infix"
                                    id="infix"
                                    type="input"
                                    ref="infixRef"
                                    defaultValue={this.props.infix}
                                    className="form-control"
                                    onKeyPress={(event) => {

                                        const target = event.target;

                                        if (event.charCode === 13) { //event.key='Enter' Only process after ENTER is pressed
                                            const value = target.type === "checkbox" ? target.checked : target.value;
                                            const name = target.name;

                                            console.log(name + " : " + value);

                                            //Perform Search and filter
                                            this.handleExpressionEval(event);
                                        }
                                    }
                                    }
                                />
                            </td>
                             
                            <td>
                                <button className="btn btn-success" onClick={(e) => this.handleExpressionEval(e)}>Eval</button>
                                <div ref="errorRef" style={{ display: 'none', marginTop: spacing + 'px', color: color }}>
                                    <strong>Invalid Expression</strong>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label>Result:</label>
                            </td>
                            <td>
                                <strong>{this.props.result}</strong>
                          
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Error:</label>
                            </td>
                            <td>
                                {this.props.error != null ?  
                                    <div style={{ color: color }}>
                                        {this.props.error}
                                    </div>
                                : null 
                                }
                                
                                    
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Dependencies Injection Description:</label>
                            </td>
                            <td>
                                {this.props.description }
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div >
        )
    }

    handleExpressionEval(e) {
        console.log("called handleExpressionEval");

        const infixRefInput = this.refs.infixRef;
        const errorRefDiv = this.refs.errorRef;

        const text = infixRefInput.value.trim();
         
        console.log("text:" + text);

        //Can do some client side regex validation here
        var regexPat = new RegExp('\\s*([.)(-+/*0-9])\\s*', 'g');
        if (regexPat.test(text) === false) {
            errorRefDiv.style.display = '';
        } else {
            errorRefDiv.style.display = 'none';
            this.props.requestCalcEval(text);
        }

        
        
    }
}
 

export default connect(
    state => state.simpleCalculator,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(SimpleCalculator);
