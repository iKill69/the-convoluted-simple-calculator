﻿import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import SimpleCal from './components/SimpleCalculator';

export default () => (
  <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/simplecalculator' component={SimpleCal} />
 
  </Layout>
);
