﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Expressions.Interfaces;
using WebCalculator.POCO;
using Newtonsoft.Json;

namespace WebCalculator.Controllers
{
     
    [Produces("application/json")]
    [Route("api/[controller]")]
    //[ApiController]
    public class SimpleCalculatorController : Controller // ControllerBase
    {
        private readonly IExpression expCalc;
         
        public SimpleCalculatorController(IExpression ExpCalc)
        {
            expCalc = ExpCalc;
        }

        // POST: api/SimpleCalculator
        [HttpPost("[action]")]
        public IActionResult EvalExpression([FromBody] ExpRequest expRequest)
        {
            ExpResult r = new ExpResult();
            try
            {
                r.Description = expCalc.Description;
                r.Result = expCalc.Calc(expRequest.infix);
                return Ok(r);
            }
            catch (Exception ex)
            {
                
                r.Error = ex.Message; //VERY BAD FOR SECURITY EXAMPLE ONLY
                return BadRequest(r);
            }
        }
         
    }
}
