﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebCalculator.POCO
{
    public class ExpResult
    {
        public string Result { get; set; }
        public string Description { get; set; }
        public string Error { get; set; }
    }
}
