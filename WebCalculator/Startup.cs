using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Expressions.Interfaces;
using Expressions;
using Expressions.ThirdParty;
using Expressions.Online;
using WebCalculator.POCO;

namespace WebCalculator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //I could use reflection to look in a specifice DIR for the plugable assemblies 
            //that implement the required IExpression interface and are specified for use in the config 
            //I am not going to bother for this example 
            // this demonstrates dependency injection well enough for this example
            var mode = Configuration.GetSection("ExpressionConfig").GetValue<string>("Lib");
            switch (mode.ToLower().Trim())
            {
                case "online":
                    services.AddTransient<IExpression, OnlineExpression>();
                    break;
                case "3rd":
                    services.AddTransient<IExpression, ThirdPartyExpression>();
                    break;
                case "mysimple":
                default:
                    services.AddTransient<IExpression, MySimpleExpression>();
                    break;
            }
              
            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
